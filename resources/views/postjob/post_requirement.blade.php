<div class="clear"> </div>
    <div class="box-03 container-fluid">
      <div class="heading">
        <h3> Post Requirement </h3>
      </div>
                
                <div class="form">
                <form>
							
							<fieldset class="col-md-6">
                            
								<div class="form-group">
									<label>Job Role</label>
									<input class="form-control" placeholder="Job Role" type="text">
								</div>
                                <div class="form-group">
									<label>Duties/Responsibilty</label>
									<input class="form-control" placeholder="Duties/Responsibilty" type="text">
								</div>
                                <div class="form-group">
									<label>Desired Qualification</label>
									<input class="form-control" placeholder="Desired Qualification" type="text">
								</div>
                                <div class="form-group">
									<label>Desired Experience</label>
									<input class="form-control" placeholder="Desired Experience" type="text">
								</div>
                                
                                <div class="form-group">
									<label>Desired Industry</label>
									<input class="form-control" placeholder="Desired Industry" type="text">
								</div>
                                
                                <div class="form-group three-inputs multinput">
									<label>Location</label>
									<input class="form-control" type="text" placeholder="Country"/> 
                        			<input class="form-control" type="text" placeholder="Province" /> 
                            		<input class="form-control" type="text" placeholder="City" /> 
								</div>
                                </fieldset>
                                <fieldset class="col-md-6">
                                
                                <div class="form-group three-inputs multinput">
									<label>Salary Offered</label>
									<input class="form-control" type="text" placeholder="Range" /> 
                        			<input class="form-control" type="text" placeholder="From"/> 
                            		<input class="form-control" type="text" placeholder="To"/> 
								</div>
                                <div class="form-group">
									<label>Commission Offered</label>
									<input class="form-control"  placeholder="Per candidate on sucessful hiring" type="text">
								</div>
                                <div class="form-group two-inputs multinput">
									<label>Action</label>
									<input class="form-control" type="text" placeholder="Make Offer"/> 
                           			<input class="form-control" type="text" placeholder="Request Bid"/>
								</div>
                                <div class="form-group">
									<label>Share</label>
									<input class="form-control" type="text" placeholder="Save Drafts"/> 
								</div>
                                <div class="form-group">
									<label>Valid Till</label>
									<input class="form-control" type="text" placeholder="Date"/> 
								</div>
                                <div class="form-group">
									<label>Special Instuction</label>
									<input class="form-control" type="text" placeholder="Special Instuction"/> 
								</div>
                                 
							</fieldset>
							<div class="form-actions">
								<a href="javascript:submitform('post_requirement1');"><div class="btn btn-primary btn-lg">
									<i class="fa fa-save"></i>
									Post
								</div></a>
								
								
							</div>
						</form>
                    
                </div>
                
    		</div>
			