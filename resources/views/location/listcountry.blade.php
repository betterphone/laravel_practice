<div class="box-03 container-fluid">
      <div class="heading">
        <h3>Manage Country <small class="edit"><i class="fa fa-pencil" aria-hidden="true"></i></small> <small class="delete"><i class="fa fa-trash-o" aria-hidden="true"></i> </small> </h3>
      </div>
      <div class="form Manage-text-table">
	  		 @if(Session::has('flash_message'))
				    <div class="alert alert-success">
				    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span></button>
				        {{ Session::get('flash_message') }}
				    </div>
	@endif
        <form>
          <table class="table">
  <thead class="thead-default">
    <tr>

      <th>First Name</th>
      <th>Last Name</th>
    
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
     <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
  @foreach($country as $country)
    <tr>
      <td>{{$country->name}}</td>
      <td>{{$country->slug}}</td>
    
      <td><ul class="list-inline">
      <li><input onclick="updatefunction('updatecountry','activeLocation','{{$country->id}}',this.value)" type="checkbox" @if($country->status==1) checked @endif data-toggle="toggle" data-size="mini"></li>      
      <li><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
      <li><a href="javascript:editDeletefunction('addcountry','activeLocation','{{$country->id}}');"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></li>
      <li><a href="javascript:editDeletefunction('deletecountry','activeLocation','{{$country->id}}');"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
      
      </ul> </td>
    </tr>
@endforeach	
   

  </tbody>
</table>
<div class="row">
<div class="col-md-12">
<a href="javascript:menulink('addcountry','activeLocation');" class="addUser"><i class="fa fa-user-plus" aria-hidden="true"></i> Add Country</a>
</div>
</div>
        </form>
      </div>
    </div>