         <div class="clear"> </div>
    <div class="box-03 container-fluid">
      <div class="heading">
        <h3>Add Country </h3>
      </div>         
                <div class="form">
							<div>
								<a href="javascript:menulink('country','activeLocation');"><div class="btn btn-primary">
									<i class="fa fa-save"></i>
									Back
								</div></a>
								
								
							</div>
            {{ Form::open(array('method'=>'post','id'=>'countryForm')) }}
						
							<fieldset class="col-md-6">
                           <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
							 <input type="hidden" name="ID" value="{{ isset($country->id) ? $country->id : null }}">
								<div class="form-group">
									<label>Add Country</label>
									<input class="form-control" name="name" placeholder="Country Name" value="{{ isset($country->name) ? $country->name : null}}" type="text">
							
								</div>
                     	<div class="form-actions">
								<a href="javascript:submitform('savecountry');"><div class="btn btn-primary btn-lg">
									<i class="fa fa-save"></i>
									{{ isset($country->name) ? 'Edit Contry' : 'Add Contry' }}
								</div></a>
								
								
							</div>
                                </fieldset>   
						
						  {{ Form::close() }}
                    
                </div>
                
    		</div>
		