   <div class="box-2 col-md-12">
    	<div class="box-head">
        	<div class="icn-02"> <img src="{{URL::asset('assets/images/home.png')}}" /> </div>
            <div class="head-title"> DashBoard</div>           
            <div class="head-title"> <span><img src="{{URL::asset('assets/images/next.png')}}" /> My Dashboard </span> </div>
        </div>
    </div>
    <div class="clear"> </div>
    
    		<div class="box-03 container-fluid">
            	<div class="heading-center">
                	<h3> Add Province </h3>
                </div>	
                
                <div class="form">
                <form>
							
							<fieldset class="col-md-6">
                             <input  type="hidden" name="_token" id="_token" content="{!! csrf_token() !!}"/>
							 <input class="form-control" name="status" value='0' type="hidden">
							     <div class="form-group">
									  <select name="country" class="select form-control" onchange="getresult(this.value,'Getprovince','ProvinceDiv')">
									  <option value="" selected="">Select Country</option>
										@foreach($countryName as $countryName)
												<option value="{{$countryName->id}}">{{$countryName->name}} </option>
												@endforeach
											</select>
                                  </div>
								   <div class="form-group">
								   <div id="ProvinceDiv">
									  <select name="province" class="select form-control" >
									  <option value="" selected="">Select Country</option>
									
											</select>
                                  </div>
								  </div>
								<div class="form-group">
									<label> City</label>
									<input class="form-control" name="name" placeholder="Province Name" type="text">
							
								</div>
                     	<div class="form-actions">
								<a href="javascript:submitform('savecity');"><div class="btn btn-primary btn-lg">
									<i class="fa fa-save"></i>
									Add City
								</div></a>
								
								
							</div>
                                </fieldset>   
						
						</form>
                    
                </div>
                
    		</div>
			