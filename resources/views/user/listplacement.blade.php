<div class="box-03 container-fluid">
      <div class="heading">
        <h3>{{ $pageTitle}} <small class="edit"><i class="fa fa-pencil" aria-hidden="true"></i></small> <small class="delete"><i class="fa fa-trash-o" aria-hidden="true"></i> </small> </h3>
      </div>
      <div class="form Manage-text-table">
	  		 @if(Session::has('flash_message'))
				    <div class="alert alert-success">
				    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span></button>
				        {{ Session::get('flash_message') }}
				    </div>
	@endif
        <form>


<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#pending">Pending</a></li>
    <li><a data-toggle="tab" href="#Weating">On-Hold</a></li>
  <li><a data-toggle="tab" href="#Approved">Approved</a></li>
  <li><a data-toggle="tab" href="#Rejected">Rejected</a></li>

</ul>

<div class="tab-content">
		<div id="pending" class="tab-pane fade in active">
				
			<table class="table">
				<thead class="thead-default">
					<tr>
						<th>Business Name</th>
						<th>Location</th>
						<th>Authorised Representative</th>
						<th>Date/Time</th>
						<th>Email</th>
						<th>Action</th>
					</tr>
				</thead>
			<tbody>
					<input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
					  @foreach($placementpanding as $placementpanding)
						<tr>
							  <td>{{$placementpanding->companyName}}</td>
							  <td>
							
							<?php   $country = \App\Country::where('id','=',$placementpanding['countryId'])->get();
							
							if($placementpanding['cityID']!='0')
							{
								$city = \App\City::where('id','=',$placementpanding['cityID'])->get();
							
									echo $city[0]['name'].",";
							}
							echo $country[0]['name']; 
							?>
							 
							
							
							  </td>  
							  <td>{{$placementpanding->fname}} {{$placementpanding->lname}}</td>
							 <td>{{$placementpanding->currentdate}}</td>   
							 <td @if($placementpanding->verify == '') bgcolor="green" @endif >{{$placementpanding->emailId}}</td>  
							  <td><ul class="list-inline">
							  <li><input onclick="updatefunction('updatecountry','activeUser','{{$placementpanding->id}}',this.value)" type="checkbox" ></li>      
							  <li><a href="#"><i onclick="viewmodal('{{$placementpanding->id}}','viewplacement','getUserDetail','myModal','activeUser')" class="fa fa-eye" aria-hidden="true"></i></a></li>
							  <li><a href="javascript:editDeletefunction('deletecountry','activeUser','{{$placementpanding->id}}');"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
							  </ul> </td>
						</tr>
				@endforeach	
				  </tbody>
			</table>
		</div>
		<div id="Approved" class="tab-pane fade">
				
			<table class="table">
				<thead class="thead-default">
					<tr>
						<th>Business Name</th>
						<th>Location</th>
						<th>Authorised Representative</th>
						<th>Date/Time</th>
						<th >Email</th>
						<th>Action</th>
					</tr>
				</thead>
			<tbody>
					<input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
					  @foreach($placementApproved as $placement)
						<tr>
							<td>{{$placement->companyName}}</td>
							  <td>
							
							<?php  

							$country1 = \App\Country::where('id','=',$placement['countryId'])->get();
						
						 	if($placement['cityID']!='0')
							{
								$city1 = \App\City::where('id','=',$placement['cityID'])->get();
							
									echo $city1[0]['name'].","; 
							}
							echo $country1[0]['name']; 
							?>
							 
							
							
							  </td>  
							  <td>{{$placement->fname}} {{$placement->lname}}</td>
							 <td>{{$placement->currentdate}}</td>   
							 <td @if($placement->verify == '') bgcolor="green" @endif>{{$placement->emailId}}</td>
							
							  <td><ul class="list-inline">
							  <li><input onclick="updatefunction('updatecountry','activeUser','{{$placement->id}}',this.value)" type="checkbox" ></li>      
							  <li><a href="#"><i onclick="viewmodal('{{$placement->id}}','viewplacement','getUserDetail','myModal','activeUser')" class="fa fa-eye" aria-hidden="true"></i></a></li>
							  <li><a href="javascript:editDeletefunction('deletecountry','activeUser','{{$placement->id}}');"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
							  </ul> </td>
						</tr>
				@endforeach	
				  </tbody>
			</table>
		</div>
    
	 <div id="Rejected" class="tab-pane fade">
				
			<table class="table">
				<thead class="thead-default">
					<tr>
						<th>Business Name</th>
						<th>Location</th>
						<th>Authorised Representative</th>
						<th>Date/Time</th>
						<th>Email</th>
						<th>Action</th>
					</tr>
				</thead>
			<tbody>
					<input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
					  @foreach($placementRejected as $placementRejected)
						<tr>
							 <td>{{$placementRejected->companyName}}</td>
							  <td>
							
							<?php   $country2 = \App\Country::where('id','=',$placementRejected['countryId'])->get();
							
							if($placementRejected['cityID']!='0')
							{
								$city2 = \App\City::where('id','=',$placementRejected['cityID'])->get();
							
									echo $city2[0]['name'].",";
							}
							echo $country2[0]['name']; 
							?>
							 
							
							
							  </td>  
							  <td>{{$placementRejected->fname}} {{$placementRejected->lname}}</td>
							 <td>{{$placementRejected->currentdate}}</td>   
							 <td @if($placementRejected->verify == '') bgcolor="green" @endif >{{$placementRejected->emailId}}</td>  
							
							
							  <td><ul class="list-inline">
							  <li><input onclick="updatefunction('updatecountry','activeUser','{{$placementRejected->id}}',this.value)" type="checkbox" ></li>      
							  <li><a href="#"><i onclick="viewmodal('{{$placementRejected->id}}','viewplacement','getUserDetail','myModal','activeUser')" class="fa fa-eye" aria-hidden="true"></i></a></li>
							  <li><a href="javascript:editDeletefunction('deletecountry','activeUser','{{$placementRejected->id}}');"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
							  </ul> </td>
						</tr>
				@endforeach	
				  </tbody>
			</table>
		</div>
		
		<div id="Weating" class="tab-pane fade">
				
			<table class="table">
				<thead class="thead-default">
					<tr>
						<th>Business Name</th>
						<th>Location</th>
						<th>Authorised Representative</th>
						<th>Date/Time</th>
						<th>Email</th>
						<th>Action</th>
					</tr>
				</thead>
			<tbody>
					<input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
					  @foreach($placementWeating as $placementWeating)
						<tr>
							 <td>{{$placementWeating->companyName}}</td>
							  <td>
							
							<?php   $country3 = \App\Country::where('id','=',$placementWeating['countryId'])->get();
							
							if($placementWeating['cityID']!='0')
							{
								$city3 = \App\City::where('id','=',$placementWeating['cityID'])->get();
							
									echo $city3[0]['name'].",";
							}
							echo $country3[0]['name']; 
							?>
							 
							
							
							  </td>  
							  <td>{{$placementWeating->fname}} {{$placementWeating->lname}}</td>
							 <td>{{$placementWeating->currentdate}}</td>   
							 <td @if($placementWeating->verify == '') bgcolor="green" @endif >{{$placementWeating->emailId}}</td>  
							
							<td><ul class="list-inline">
							  <li><input onclick="updatefunction('updatecountry','activeUser','{{$placementWeating->id}}',this.value)" type="checkbox" ></li>      
							  <li><a href="#"><i onclick="viewmodal('{{$placementWeating->id}}','viewplacement','getUserDetail','myModal','activeUser')" class="fa fa-eye" aria-hidden="true"></i></a></li>
							  <li><a href="javascript:editDeletefunction('deletecountry','activeUser','{{$placementWeating->id}}');"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>
							  </ul> </td>
						</tr>
				@endforeach	
				  </tbody>
			</table>
		</div>
		 </div>
        </form>
     
	  <!-- Trigger the modal with a button -->


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" id="getUserDetail">
  
    </div>

  </div>
</div>
    </div>
