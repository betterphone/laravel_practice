<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#Details">Details</a></li>
    @if($placement->status != 'Approved')
    <li><a data-toggle="tab" href="#note">Note</a></li>
    @endif
    <button class="close" data-dismiss="modal" type="button">×</button>
</ul>
<div class="tab-content">
    <div id="Details" class="tab-pane fade in active">
        <div class="modal-header">

            <h4 class="modal-title">{{$placement->fname}} {{$placement->lname}}</h4>
        </div>
        <div class="modal-body">
            company Name:{{$placement->companyName}} </br> 
            Designation: {{$placement->designation}} </br> 
            contactNo :{{$placement->contactNo}} </br> 
            EmailId: {{$placement->emailId}} </br>
        </div>

        <div class="modal-footer">
            @if($placement->status == 'panding')   <button type="button" onclick="updateUser('{{$placement->id}}', 'updateUserStatus', 'activeUser', 'Approved')" class="btn btn-default" data-dismiss="modal"></i>Approved</button> @endif
        </div>
    </div>

    <div id="note" class="tab-pane fade">
        <div class="modal-header">

            <h4 class="modal-title">{{$placement->fname}} {{$placement->lname}}</h4>
        </div>
        <div class="modal-body">
            <textarea name="notes" id="notes" ></textarea>
        </div>
        @if($placement->status == 'panding')  
        <div class="modal-footer">

            @if($placement->status != 'Rejected')   <button type="button" onclick="updateUser('{{$placement->id}}', 'updateUserStatus', 'activeUser', 'Rejected')" class="btn btn-default" data-dismiss="modal"></i>Rejected</button> @endif
            @if($placement->status != 'Weating')   <button type="button" onclick="updateUser('{{$placement->id}}', 'updateUserStatus', 'activeUser', 'Weating')" class="btn btn-default" data-dismiss="modal"></i>Weating</button> @endif


        </div>
        @endif
    </div>
</div>