<meta name="_token" content="{!! csrf_token() !!}"/>
  <div class="burger-icon menu-toggel fa ">
      <div class="switch">
        <input id="cmn-toggle-1" class="burger-icon cmn-toggle cmn-toggle-round" type="checkbox" checked>
        <label for="cmn-toggle-1"></label>
      </div>
    </div>
    <div class="nav-left">
      <div class="nav-side-menu"> 
        <div class="menu-list">
          <ul id="sideMenu" class="menu-content">
            <li class="active"> <a href="dashboard.html"> <i class="fa fa-home fa-lg"></i> Dashboard </a> </li>
            <li> <a href="signup-form.html"><i class="fa fa-user-plus" aria-hidden="true"></i> Sign up </a> </li>
            <li> <a href="manage.html"><i class="fa fa-users" aria-hidden="true"></i> Manage</a> </li>
            <li class="panel"> 
              <a href="#products" data-toggle="collapse" data-parent="#sideMenu">
              <i class="fa fa-lg fa-fw fa-cube txt-color-blue"></i> SmartAdmin Intel <span class="arrow"></span></a> 
           
            <ul class="sub-menu collapse" id="products">
              <li class="active"><a href="#">Analytics Dashboard</a></li>
              <li><a href="#">Social Wall</a></li>
            </ul>
             </li>
            <li class="panel"> 
            <a href="#service" data-toggle="collapse" data-parent="#sideMenu">
            <i class="fa fa-lg fa-fw fa-inbox"></i> Outlook <span class="arrow"></span></a> 
            <ul class="sub-menu collapse" id="service">
              <li>New Service 1</li>
              <li>New Service 2</li>
              <li>New Service 3</li>
            </ul>
            </li>
			      <li class="panel"> 
            <a href="#location" data-toggle="collapse" data-parent="#sideMenu"><i class="fa fa-gift fa-lg"></i> Location<span class="arrow"></span></a> 
            <ul class="sub-menu collapse" id="location">
             <li class="active"> <a href="javascript:menulink('country','activeLocation');">  Country  </a></li>
              <li><a href="javascript:menulink('province','activeLocation');">  Province  </a></li>
              <li><a href="javascript:menulink('city','activeLocation');">  City  </a></li>
            </ul>
            </li> 
    <li class="panel"> 
            <a href="#userLogin" data-toggle="collapse" data-parent="#sideMenu"><i class="fa fa-gift fa-lg"></i> User Management<span class="arrow"></span></a> 
            <ul class="sub-menu collapse" id="userLogin">
             <li class="active"> <a href="javascript:menulink('placement','activeUser');">  placement User  </a></li>
              <li><a href="javascript:menulink('province','activeLocation');">  Province  </a></li>
              <li><a href="javascript:menulink('city','activeLocation');">  City  </a></li>
            </ul>
            </li> 			
			<li class="panel"> 
            <a href="#form1" data-toggle="collapse" data-parent="#sideMenu"><i class="fa fa-gift fa-lg"></i> Form <span class="arrow"></span></a> 
            <ul class="sub-menu collapse" id="form1">
              <li><a href="javascript:menulink('post_requirement','activePost');"> <img src="{{URL::asset('assets/images/1469458916_graph_up_arrow.png')}}" />Post Requirement  </a></li>
                  <li>New Service 2</li>
                  <li>New Service 3</li>
           
            </ul>
            </li>
            <li class="panel"> 
            <a href="#new" data-toggle="collapse" data-parent="#sideMenu"><i class="fa fa-bar-chart-o fa-lg"></i> Graphs <span class="arrow"></span></a> 
            <ul class="sub-menu collapse" id="new">
              <li>New Graphs 1</li>
              <li>New Graphs 2</li>
              <li>New Graphs 3</li>
            </ul>
            </li>
      
          </ul>
        </div>
      </div>
    </div>
  <script type="text/javascript">
/*************************************Account Modal Function   **********************************************/
function updateUser(userID,url,activeclass,statusType){
	
	var notes = $('textarea#notes').val();
	
	 $.ajax({
      url: '<?php echo URL::asset('/');  ?>'+url,
      type: "POST",
      data: {'id':userID,'status':statusType,'notes':notes,'_token': $('input[name=_token]').val()},
	   beforeSend: function() {
        $("#pageloader").html("<img style='width:4%' src='http://localhost/job/public/assets/images/loader3.gif'>");
    },
      success: function(data){
			  $('.menu-content li').removeClass('active');
   $('#'+activeclass).addClass('active'); 	
		
		 document.getElementById("innerPage").innerHTML =data;


      }
    });      
  }

  function viewmodal(userID,url,divID,ModalID,activeclass){
	
	 $.ajax({
      url: '<?php echo URL::asset('/');  ?>'+url,
      type: "GET",
      data: {'id':userID,'_token': $('input[name=_token]').val()},
	   beforeSend: function() {
        $("#pageloader").html("<img style='width:4%' src='http://localhost/job/public/assets/images/loader3.gif'>");
    },
      success: function(data){
			  $('.menu-content li').removeClass('active');
   $('#'+activeclass).addClass('active'); 	
		 document.getElementById(divID).innerHTML =data;
		$('#'+ModalID).modal('show');

      }
    });      
  } 
/************************************************** Manu Function ******************************************************/  
function menulink(url,activeclass){

	 $.ajax({
      url: '<?php echo URL::asset('/');  ?>'+url,
      type: "GET",
      data: {'_token': $('input[name=_token]').val()},
	   beforeSend: function() {
		
        $("#pageloader").html("<img style='width:4%' src='http://localhost/job/public/assets/images/loader3.gif'>");
    },
      success: function(data){

		  $('.menu-content li').removeClass('active');
   $('#'+activeclass).addClass('active'); 		  
		  $('#pageloader').remove();
		 document.getElementById("innerPage").innerHTML =data;

      }
    });      
  }
  
 function editDeletefunction(url,activeclass,id){
	
	 $.ajax({
      url: '<?php echo URL::asset('/');  ?>'+url,
      type: "GET",
      data: {'ID':id,'_token': $('input[name=_token]').val()},
	   beforeSend: function() {
        $("#pageloader").html("<img style='width:4%' src='http://localhost/job/public/assets/images/loader3.gif'>");
    },
      success: function(data){
		  $('.menu-content li').removeClass('active');
		$('#'+activeclass).addClass('active'); 		  
		  $('#pageloader').remove();
		 document.getElementById("innerPage").innerHTML =data;

      }
    });      
  } 
  
   function updatefunction(url,activeclass,id,status){
	alert(status);
    
  } 
  
  function submitform(url){
	  var asd= $('form').serialize()
	 $.ajax({
		  url: '<?php echo URL::asset('/');  ?>'+url,
		 type: 'POST',
      data: $('form').serialize(),
	   beforeSend: function() {
		        $("#pageloader").html("<img style='width:4%' src='http://localhost/job/public/assets/images/loader3.gif'>");
    },
      success: function(data){
		 
		 document.getElementById("innerPage").innerHTML =data;
      }
    });      
  }
  
  function getresult(id,url,divID){
	   $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
 $.ajax({
      url: '<?php echo URL::asset('/');  ?>'+url,
     	 type: 'POST',
      data: {'countrID':id,'_token': $('input[name=_token]').val()},
      success: function(data){
					 document.getElementById(divID).innerHTML =data;
      }
    }); 
  }
</script>  