<?php

namespace App\Http\Controllers;

use Auth;
use App\User as user;
use App\Properties;
use App\Country;
use App\Province;
use App\City;
use App\PlacementUser;
use App\NotesUser;
use App\RejectedallUser;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;

class UserController extends MainAdminController {

    public function __construct() {
        $this->middleware('auth');
    }

    public function placementlist() {
        $user_id = Auth::user()->id;
        $pageTitle = "Placement Company";
        $placementpanding = PlacementUser::where('status', 'panding')->orderBy('id')->get();
        $placementApproved = PlacementUser::where('status', 'Approved')->orderBy('id')->get();
        $placementRejected = PlacementUser::where('status', 'Rejected')->orderBy('id')->get();
        $placementWeating = PlacementUser::where('status', 'Weating')->orderBy('id')->get();

        return view('user.listplacement', compact('placementpanding', 'placementApproved', 'placementRejected', 'placementWeating', 'pageTitle'));
    }

    public function viewplacement(Request $request) {
        $inputs = $request->all();

        $placement = PlacementUser::findOrFail($inputs['id']);

        $user_id = Auth::user()->id;
        return view('user.viewplacement', compact('placement'));
    }

    public function updateUserStatus(Request $request) {
        $user_id = Auth::user()->id;

        $inputs = $request->all();
        if (!empty($inputs['id'])) {

            $UserNotes = new NotesUser;
            $UserNotes->userID = $inputs['id'];
            $UserNotes->UserTable = 'placementuser';
            $UserNotes->notes = $inputs['notes'];
            $UserNotes->save();
        }
        if ($inputs['status'] == 'Rejected') {
            $placement2 = PlacementUser::findOrFail($inputs['id']);
            $rejectedalluser = new RejectedallUser;
            $rejectedalluser->usertableID = $inputs['id'];
            $rejectedalluser->tablename = 'rejectedalluser';
            $rejectedalluser->fname = $placement2['fname'];
            $rejectedalluser->lname = $placement2['lname'];
            $rejectedalluser->companyName = $placement2['companyName'];
            $rejectedalluser->authorised_representative = $placement2['authorised_representative'];
            $rejectedalluser->designation = $placement2['designation'];
            $rejectedalluser->contactNo = $placement2['contactNo'];
            $rejectedalluser->countryId = $placement2['countryId'];
            $rejectedalluser->provinceID = $placement2['provinceID'];
            $rejectedalluser->cityID = $placement2['cityID'];
            $rejectedalluser->emailId = $placement2['emailId'];
            $rejectedalluser->status = $inputs['status'];
            $rejectedalluser->currentdate = $placement2['currentdate'];
            $rejectedalluser->ip = $placement2['ip'];

            if ($rejectedalluser->save()) {
                $placement3 = PlacementUser::where('id', '=', $inputs['id'])->delete();
            }
        } else {
            $placement1 = PlacementUser::findOrFail($inputs['id']);
            $placement1->status = $inputs['status'];
            if ($inputs['status'] == 'Approved') {
                $password = str_random(6);
                $username = str_random(8);
                $placement1->username = $username;
                $placement1->password = bcrypt($password);
                $placement1->Flogin = '1';

                $data = array(
                    'userName' => $username,
                    'Password' => $password,
                    'emailId' => $placement1['emailId'],
                    'url' => 'http://xeriffjobsolutions.com/adminjob/public/verifyEmail/' . $verify,
                );

                /*   Mail::send('emails.welcome',$data, function($message)  use ($data)
                  {
                  $message->from('moh.aadil1@gmail.com', 'Support');

                  $message->to($data['emailId'])->subject('Verify Email Id');


                  }); */
            }
            $placement1->save();
        }


        $pageTitle = "Placement Company";
        $placementpanding = PlacementUser::where('status', 'panding')->orderBy('id')->get();
        $placementApproved = PlacementUser::where('status', 'Approved')->orderBy('id')->get();
        $placementRejected = PlacementUser::where('status', 'Rejected')->orderBy('id')->get();
        $placementWeating = PlacementUser::where('status', 'Weating')->orderBy('id')->get();
        \Session::flash('flash_message', 'Status Changes ');

        return view('user.listplacement', compact('placementpanding', 'placementApproved', 'placementRejected', 'placementWeating', 'pageTitle'));
    }

    public function deletecountry(Request $request) {

        $country = Country::orderBy('id')->get();
        $inputs = $request->all();
        $country1 = Country::findOrFail($inputs['ID']);
        $country1->delete();

        \Session::flash('flash_message', 'Deleted');

        return view('location.listcountry', compact('country'));
    }

    public function province() {
        $countryName = Country::where('status', '1')->get();
        $user_id = Auth::user()->id;
        return view('location.provine', compact('countryName'));
    }

    public function saveprovince(Request $request) {
        $user_id = Auth::user()->id;

        $inputs = $request->all();
        $province = new Province;
        $province->name = $inputs['name'];
        $province->countryID = $inputs['country'];
        $province->status = '1';
        $province->ip = $_SERVER['REMOTE_ADDR'];
        $province->save();
        //return view('location.country');
    }

    public function city() {
        $countryName = Country::where('status', '1')->get();
        $user_id = Auth::user()->id;
        return view('location.city', compact('countryName'));
    }

    public function Getprovince(Request $request) {
        $user_id = Auth::user()->id;
        $inputs = $request->all();
        $provinceDetail = Province::where('countryID', $inputs['countrID'])->where('status', '1')->get();
        echo view('location.ajprovince', compact('provinceDetail'));
    }

    public function savecity(Request $request) {
        $user_id = Auth::user()->id;

        $inputs = $request->all();

        $city = new City;
        $city->name = $inputs['name'];
        $city->countryID = $inputs['country'];
        $city->provinceID = $inputs['provinceID'];
        $city->status = '1';
        $city->ip = $_SERVER['REMOTE_ADDR'];
        $city->save();
        //return view('location.country');
    }

    public function show_user($id) {
        $user = user::find($id);
        echo $user->name;
    }

    public function update_profile() {
        extract($_POST);
        $user_id = Auth::user()->id;
        $query = new user;
        $query = user::find($user_id);
        $query->name = $name;
        $query->email = $email;
        $query->save();
        return redirect()->to('profile')->with('data', ['updated' => 'Updated']);
    }

}
