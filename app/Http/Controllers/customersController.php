<?php

namespace App\Http\Controllers;

use App\Customers as customer;
use Illuminate\Http\Request;
use App\Http\Requests;

class customersController extends Controller {

    public function show() {
        $customers = customer::all();
        $data = array(
            'customers' => $customers
        );
        return view('customers', $data);
    }

    public function customer_by_id($id) {
        $customer = customer::find($id);
        echo $customer->name;
    }

}
