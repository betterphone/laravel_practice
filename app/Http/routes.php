<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('customers', 'customersController@show');

Route::get('customer_by_id/{id}', 'customersController@customer_by_id');

Route::get('orders', 'ordersController@show');

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('profile', 'profileController@index');

Route::post('profile', 'UserController@update_profile');

Route::get('user/{id}', 'UserController@show_user');

Route::get('form', function() {
    return view('form');
});

use Illuminate\Http\Request;

Route::post('post_to_me', function(Request $request) {
    $input = $request->all();
    $name = $request->input('name');
    print_r($input);
});

